package com.juandavid.practicademoironbit

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
/*
* Clase Application para instancia global en App
* */
@HiltAndroidApp
class PracticaDemoIronBitApplication:Application() {
}