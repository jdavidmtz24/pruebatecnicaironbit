package com.juandavid.practicademoironbit.data.model.array_movies

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleDate(
    @SerializedName("time" ) var time : String?           = null,
    @SerializedName("days" ) var days : ArrayList<String> = arrayListOf()
):Parcelable
