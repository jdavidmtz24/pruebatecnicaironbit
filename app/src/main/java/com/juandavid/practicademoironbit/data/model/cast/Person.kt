package com.juandavid.practicademoironbit.data.model.cast

import com.google.gson.annotations.SerializedName
import com.juandavid.practicademoironbit.data.model.array_movies.Country
import com.juandavid.practicademoironbit.data.model.array_movies.Image
import com.juandavid.practicademoironbit.data.model.array_movies.Links

data class Person (

    @SerializedName("id"       ) var id       : Int?     = null,
    @SerializedName("url"      ) var url      : String?  = null,
    @SerializedName("name"     ) var name     : String?  = null,
    @SerializedName("country"  ) var country  : Country? = Country(),
    @SerializedName("birthday" ) var birthday : String?  = null,
    @SerializedName("deathday" ) var deathday : String?  = null,
    @SerializedName("gender"   ) var gender   : String?  = null,
    @SerializedName("image"    ) var image    : Image?   = Image(),
    @SerializedName("updated"  ) var updated  : Int?     = null,
    @SerializedName("_links"   ) var Links    : Links?   = Links()

)
