package com.juandavid.practicademoironbit.data.model.array_movies

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WebChanel(@SerializedName("id") var id: Int? = null,
                     @SerializedName("name") var name: String? = null,
                     @SerializedName("country") var country: Country? = null,
                     @SerializedName("officialSite") var officialSite: String? = null,
):Parcelable
