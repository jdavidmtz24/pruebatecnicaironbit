package com.juandavid.practicademoironbit.data.model.cast

import com.google.gson.annotations.SerializedName

data class CastActors(@SerializedName("person"    ) var person    : Person?    = Person(),
                      @SerializedName("character" ) var character : Character? = Character(),
                      @SerializedName("self"      ) var self      : Boolean?   = null,
                      @SerializedName("voice"     ) var voice     : Boolean?   = null)
