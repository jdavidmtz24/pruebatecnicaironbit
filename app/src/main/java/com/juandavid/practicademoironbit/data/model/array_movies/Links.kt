package com.juandavid.practicademoironbit.data.model.array_movies

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Links (@SerializedName("self"            ) var self            : Self?            = Self(),
                  @SerializedName("previousepisode" ) var previousepisode : Previousepisode? = Previousepisode(),
                  @SerializedName("nextepisode"     ) var nextepisode     : Nextepisode?     = Nextepisode()
):Parcelable
