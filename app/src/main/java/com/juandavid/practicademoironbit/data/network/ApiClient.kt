package com.juandavid.practicademoironbit.data.network

import com.juandavid.practicademoironbit.data.model.array_movies.Movies
import com.juandavid.practicademoironbit.data.model.cast.CastActors
import com.juandavid.practicademoironbit.data.model.movie.Movie
import com.juandavid.practicademoironbit.data.model.movie_search.MovieQuery
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query
/*
* Interface Endpoints para consumo en App
* */
interface ApiClient {


    @Headers("Content-Type: application/json")
    @GET("schedule")
    suspend fun getMoviesArray(@Query("country") country:String,@Query("date") date:String): Response<ArrayList<Movies>>

    @Headers("Content-Type: application/json")
    @GET("search/shows")
    suspend fun getMoviesQuery(@Query("q") query:String): Response<ArrayList<MovieQuery>>


    @Headers("Content-Type: application/json")
    @GET("shows/{idMovie}")
    suspend fun getMovie(@Path("idMovie") idMovie:Int): Response<Movie>

    @Headers("Content-Type: application/json")
    @GET("shows/{idMovie}/cast")
    suspend fun getMovieCast(@Path("idMovie") idMovie:Int): Response<ArrayList<CastActors>>
}