package com.juandavid.practicademoironbit.data.network

import com.juandavid.practicademoironbit.data.model.array_movies.Movies
import com.juandavid.practicademoironbit.data.model.cast.CastActors
import com.juandavid.practicademoironbit.data.model.movie.Movie
import com.juandavid.practicademoironbit.data.model.movie_search.MovieQuery
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/*
* Clase Service para retorno de Respues a Endpoints en App
* */
class MovieService @Inject constructor(private val apiClient: ApiClient){

    suspend fun getMovies(country:String, date:String): ArrayList<Movies>?{
        return withContext(Dispatchers.IO){
            val response= apiClient.getMoviesArray(country,date)
            if (response.code() == 200 ) response.body() else null
        }
    }

    suspend fun getMoviesQuery(query:String):ArrayList<MovieQuery>?{
        return withContext(Dispatchers.IO){
            val response = apiClient.getMoviesQuery(query)
            if (response.code() == 200) response.body() else null
        }
    }

    suspend fun getMovieId(idMovie: Int):Movie?{
        return withContext(Dispatchers.IO){
            val response = apiClient.getMovie(idMovie)
            if (response.code() == 200) response.body() else null
        }
    }

    suspend fun getCastMovieId(idMovie: Int):ArrayList<CastActors>?{
        return withContext(Dispatchers.IO){
            val response = apiClient.getMovieCast(idMovie)
            if (response.code() == 200) response.body() else null
        }
    }
}