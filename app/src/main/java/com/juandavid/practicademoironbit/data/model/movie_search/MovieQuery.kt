package com.juandavid.practicademoironbit.data.model.movie_search

import com.google.gson.annotations.SerializedName
import com.juandavid.practicademoironbit.data.model.array_movies.Show

data class MovieQuery(@SerializedName("score" ) var score : Double? = null,
                      @SerializedName("show"  ) var show  : Show?   = Show())
