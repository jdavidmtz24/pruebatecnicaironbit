package com.juandavid.practicademoironbit.data.model.array_movies

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ShowLink(
    @SerializedName("self" ) var self : Self? = Self(),
    @SerializedName("show" ) var show : ShowLinkHref? = ShowLinkHref(),

    ):Parcelable