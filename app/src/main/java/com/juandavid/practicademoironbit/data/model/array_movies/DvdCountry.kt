package com.juandavid.practicademoironbit.data.model.array_movies

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DvdCountry(@SerializedName("name") var name: String? = null,
                      @SerializedName("code") var code: String? = null,
                      @SerializedName("timezone") var timezone: String? = null):Parcelable
