package com.juandavid.practicademoironbit.data.model.array_movies

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Externals(
    @SerializedName("tvrage") var tvrage: Int? = null,
    @SerializedName("thetvdb") var thetvdb: Int? = null,
    @SerializedName("imdb") var imdb: String? = null
):Parcelable
