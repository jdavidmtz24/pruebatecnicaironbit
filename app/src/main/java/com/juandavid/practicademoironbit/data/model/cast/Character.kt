package com.juandavid.practicademoironbit.data.model.cast

import com.google.gson.annotations.SerializedName
import com.juandavid.practicademoironbit.data.model.array_movies.Image
import com.juandavid.practicademoironbit.data.model.array_movies.Links

data class Character (

    @SerializedName("id"     ) var id    : Int?    = null,
    @SerializedName("url"    ) var url   : String? = null,
    @SerializedName("name"   ) var name  : String? = null,
    @SerializedName("image"  ) var image : Image?  = Image(),
    @SerializedName("_links" ) var Links : Links?  = Links()

)
