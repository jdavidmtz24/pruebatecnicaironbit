package com.juandavid.practicademoironbit.data

import com.juandavid.practicademoironbit.data.model.CastActorProvider
import com.juandavid.practicademoironbit.data.model.MovieProvider
import com.juandavid.practicademoironbit.data.model.MoviesProvider
import com.juandavid.practicademoironbit.data.model.MoviesQueryProvider
import com.juandavid.practicademoironbit.data.model.array_movies.Movies
import com.juandavid.practicademoironbit.data.model.cast.CastActors
import com.juandavid.practicademoironbit.data.model.movie.Movie
import com.juandavid.practicademoironbit.data.model.movie_search.MovieQuery
import com.juandavid.practicademoironbit.data.network.MovieService
import javax.inject.Inject
/*
* Clase Repository para almacenamiento de Datos en Sesion en App
* */
class MovieRepository @Inject constructor(private val service: MovieService) {

    suspend fun getMoviesArray(country: String, date:String):ArrayList<Movies>?{
        val response = service.getMovies( country, date)
        MoviesProvider.arrayMovies = response
        return response
    }

    suspend fun getMoviesQuery(query: String):ArrayList<MovieQuery>?{
        val response = service.getMoviesQuery(query)
        MoviesQueryProvider.arrayMoviesQuery = response
        return response
    }

    suspend fun getMovieId(idMovie: Int):Movie?{
        val response = service.getMovieId(idMovie)
        MovieProvider.movie = response
        return response
    }

    suspend fun getCastMovieID(idMovie: Int):ArrayList<CastActors>?{
        val response = service.getCastMovieId(idMovie)
        CastActorProvider.arrayCastActors = response
        return response
    }

}