package com.juandavid.practicademoironbit.data.model.array_movies

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Previousepisode(
    @SerializedName("href" ) var href : String? = null
):Parcelable
