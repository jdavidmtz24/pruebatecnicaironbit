package com.juandavid.practicademoironbit.domain

import com.juandavid.practicademoironbit.data.MovieRepository
import com.juandavid.practicademoironbit.data.model.array_movies.Movies
import com.juandavid.practicademoironbit.utls.COUNTRY
import com.juandavid.practicademoironbit.utls.getDate
import javax.inject.Inject
/*
* Clase UsesCase para invocacion de respuesta de
* Show o Peliculas presentados en el dia
* */
class GetMoviesArrayUsesCase @Inject constructor(private val repository: MovieRepository) {

    suspend operator fun invoke(): ArrayList<Movies>? = repository.getMoviesArray(COUNTRY, getDate())

}