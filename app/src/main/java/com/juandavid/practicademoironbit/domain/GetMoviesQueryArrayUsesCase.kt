package com.juandavid.practicademoironbit.domain

import com.juandavid.practicademoironbit.data.MovieRepository
import com.juandavid.practicademoironbit.data.model.movie_search.MovieQuery
import javax.inject.Inject
/*
* Clase UsesCase para invocacion de respuesta de
* Show o Peliculas por coincidencia de Texto
* */
class GetMoviesQueryArrayUsesCase @Inject constructor(private val repository: MovieRepository) {

    suspend operator fun invoke(query:String): ArrayList<MovieQuery>? = repository.getMoviesQuery(query)
}