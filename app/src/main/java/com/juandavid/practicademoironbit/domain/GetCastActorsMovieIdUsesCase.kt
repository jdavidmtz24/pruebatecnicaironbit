package com.juandavid.practicademoironbit.domain

import com.juandavid.practicademoironbit.data.MovieRepository
import com.juandavid.practicademoironbit.data.model.cast.CastActors
import javax.inject.Inject
/*
* Clase UsesCase para invocacion de respuesta de
* Casting de Actores de Show o Pelicula
* */
class GetCastActorsMovieIdUsesCase @Inject constructor(private val repository: MovieRepository) {
    
    suspend operator fun invoke(idMovie: Int): ArrayList<CastActors>? = repository.getCastMovieID(idMovie)
}