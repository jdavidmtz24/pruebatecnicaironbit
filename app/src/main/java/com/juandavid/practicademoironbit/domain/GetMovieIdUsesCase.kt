package com.juandavid.practicademoironbit.domain

import com.juandavid.practicademoironbit.data.MovieRepository
import javax.inject.Inject

/*
* Clase UsesCase para invocacion de respuesta de
* Detalle de Show o Pelicula
* */

class GetMovieIdUsesCase @Inject constructor(private val repository: MovieRepository) {

    suspend operator fun invoke(idMovie:Int) = repository.getMovieId(idMovie)
}