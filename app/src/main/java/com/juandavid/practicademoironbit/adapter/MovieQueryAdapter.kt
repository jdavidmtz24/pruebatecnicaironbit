package com.juandavid.practicademoironbit.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.practicademoironbit.R
import com.juandavid.practicademoironbit.data.model.array_movies.Movies
import com.juandavid.practicademoironbit.data.model.movie_search.MovieQuery
import com.juandavid.practicademoironbit.utls.getStringFromHTML
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

class /*
* Adaptador Vista de Shows por coincidencia de Texto
* */
MovieQueryAdapter(arrayList:ArrayList<MovieQuery>, private val onClickListener: OnClickListener):RecyclerView.Adapter<MovieQueryAdapter.ViewHolder>() {
    private val movieQueryArrayList: ArrayList<MovieQuery>
    class ViewHolder(item: View): RecyclerView.ViewHolder(item){
        val itemImageMovie: ImageView
        val itemTitleMovie: TextView
        val itemProducerMovie: TextView
        val itemDateMovie: TextView
        val itemProgressBar:ProgressBar
        init{
            itemImageMovie = item.findViewById(R.id.imageview_movie_item)
            itemTitleMovie = item.findViewById(R.id.textview_title_movie)
            itemProducerMovie = item.findViewById(R.id.textview_producer_movie)
            itemDateMovie = item.findViewById(R.id.textview_date_movie)
            itemProgressBar = item.findViewById(R.id.progress_item)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieQueryAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieQueryAdapter.ViewHolder, position: Int) {
        holder.itemProgressBar.isVisible = true
        val modelMoviesQuery: MovieQuery =  movieQueryArrayList[position]
        try {
            val urlImageMovie = if (!modelMoviesQuery.show!!.image!!.medium.isNullOrBlank()) modelMoviesQuery.show!!.image!!.medium else modelMoviesQuery.show!!.image!!.original
            Picasso.get().load(urlImageMovie).into(holder.itemImageMovie, object : Callback{
                override fun onSuccess() {
                    holder.itemProgressBar.isVisible =  false
                }

                override fun onError(e: Exception?) {
                    holder.itemProgressBar.isVisible =  false
                }
            })
            holder.itemTitleMovie.text =  modelMoviesQuery.show!!.name
            holder.itemProducerMovie.text =textProducer(modelMoviesQuery)
            val horario = "${modelMoviesQuery.show!!.schedule!!.time} | ${modelMoviesQuery.show!!.schedule?.days.toString().replace("[","").replace("]","")}"//${modelMoviesQuery.show!!.schedule!!.time} | ${modelMoviesQuery.show!!.schedule?.days.toString()}"
            holder.itemDateMovie.text= horario
            holder.itemView.setOnClickListener {
                onClickListener.clickListener(modelMoviesQuery)
            }
        } catch (nullpointer:java.lang.NullPointerException) {
            holder.itemProgressBar.isVisible = false
        }
    }

    override fun getItemCount(): Int {
       return movieQueryArrayList.size
    }
    private fun textProducer(modelMovies: MovieQuery): String? {
        return if(!modelMovies.show!!.network!!.name.isNullOrBlank())
            modelMovies.show!!.network!!.name
        else if(!modelMovies.show!!.webChannel!!.name.isNullOrBlank())
            modelMovies.show!!.webChannel!!.name
        else
            " "
    }
    init {
        this.movieQueryArrayList = arrayList
    }

     class OnClickListener(val clickListener: (modelMovieQuery: MovieQuery) -> Unit){
        fun onClick(modelMovieQuery: MovieQuery)= clickListener(modelMovieQuery)
    }

}