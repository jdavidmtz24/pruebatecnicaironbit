package com.juandavid.practicademoironbit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.practicademoironbit.R
import com.juandavid.practicademoironbit.data.model.cast.CastActors
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception
/*
* Adaptador vista de Actores
* */
class CastMovieAdapter(arrayCastMoview: ArrayList<CastActors>) :
    RecyclerView.Adapter<CastMovieAdapter.ViewHolder>() {
    private val castActorArray: ArrayList<CastActors>

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val itemImageCastMovie: ImageView
        val itemTextNameCastMovie: TextView
        val itemProgressCastMovie: ProgressBar

        init {
            itemImageCastMovie = item.findViewById(R.id.image_cast_movie_item)
            itemTextNameCastMovie = item.findViewById(R.id.textview_name_cast_item)
            itemProgressCastMovie = item.findViewById(R.id.progress_cast_item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastMovieAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_cast_movie, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return castActorArray.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemProgressCastMovie.isVisible = true
        val castActor: CastActors = castActorArray[position]
        try {
            val urlImageCastMovie = castActor.person!!.image!!.medium
            Picasso.get().load(urlImageCastMovie)
                .into(holder.itemImageCastMovie, object : Callback {
                    override fun onSuccess() {
                        holder.itemProgressCastMovie.isVisible = false
                    }

                    override fun onError(e: Exception?) {
                        holder.itemProgressCastMovie.isVisible = false
                    }
                })
            holder.itemTextNameCastMovie.text =
                if (castActor.person!!.name != null) castActor.person!!.name else ""
        } catch (nullpointer: java.lang.NullPointerException) {
            holder.itemProgressCastMovie.isVisible = false
        }
    }

    init {
        this.castActorArray = arrayCastMoview
    }
}