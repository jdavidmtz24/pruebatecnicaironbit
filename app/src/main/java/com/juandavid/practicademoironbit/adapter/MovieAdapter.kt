package com.juandavid.practicademoironbit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.practicademoironbit.R
import com.juandavid.practicademoironbit.data.model.array_movies.Movies
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception
/*
* Adaptador Vista de Shows
* */
class MovieAdapter(arrayMovie: ArrayList<Movies>, private val onClickListener: OnClickListener): RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    private val modelMovieArray: ArrayList<Movies>

    init {
        this.modelMovieArray = arrayMovie
    }
    class ViewHolder(item: View): RecyclerView.ViewHolder(item){
        val itemImageMovie: ImageView
        val itemTitleMovie: TextView
        val itemProducerMovie: TextView
        val itemDateMovie: TextView
        val itemProgressBar:ProgressBar
        init{
            itemImageMovie = item.findViewById(R.id.imageview_movie_item)
            itemTitleMovie = item.findViewById(R.id.textview_title_movie)
            itemProducerMovie = item.findViewById(R.id.textview_producer_movie)
            itemDateMovie = item.findViewById(R.id.textview_date_movie)
            itemProgressBar = item.findViewById(R.id.progress_item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return modelMovieArray.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemProgressBar.isVisible = true
        val modelMovies: Movies =  modelMovieArray[position]
        try {
            val urlImageMovie = if (!modelMovies.show!!.image!!.medium.isNullOrBlank()) modelMovies.show!!.image!!.medium else modelMovies.show!!.image!!.original
            Picasso.get().load(urlImageMovie).into(holder.itemImageMovie, object : Callback{
                override fun onSuccess() {
                    holder.itemProgressBar.isVisible =  false
                }

                override fun onError(e: Exception?) {
                    holder.itemProgressBar.isVisible =  false
                }
            })
            holder.itemTitleMovie.text =  modelMovies.show!!.name
            holder.itemProducerMovie.text = textProducer(modelMovies)
            holder.itemDateMovie.text= "${modelMovies.airdate} | ${modelMovies.airtime}"
            holder.itemView.setOnClickListener {
                onClickListener.onClick(modelMovies)
            }
        } catch (nullpointer:java.lang.NullPointerException) {
            holder.itemProgressBar.isVisible = false
        }
    }

    private fun textProducer(modelMovies: Movies): String? {
        return if(!modelMovies.show!!.network!!.name.isNullOrBlank())
            modelMovies.show!!.network!!.name
        else if(!modelMovies.show!!.webChannel!!.name.isNullOrBlank())
            modelMovies.show!!.webChannel!!.name
        else
            " "
    }

    class OnClickListener(val clickListener: (arrayReference: Movies) -> Unit){
        fun onClick(arrayReference: Movies)= clickListener(arrayReference)
    }
}