package com.juandavid.practicademoironbit.utls

import android.text.Html
import android.text.Spanned
import java.text.SimpleDateFormat
import java.util.*

/*
* Metodos genericos en App
* */
fun getDate(): String {
    val formatter = SimpleDateFormat("yyyy-MM-dd")
    val date = Date()
    return formatter.format(date)
}
fun getStringFromHTML(text:String):Spanned{
    return Html.fromHtml(text)
}
fun getDateMobile():String{
    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)

    val dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
    val dayOfMonth = if (day<=9) "0$day" else day
    val dayWeekName = getDateWeek(dayOfWeek)
    val monthName = getMonth(month)
    return "$dayWeekName $dayOfMonth de $monthName del $year"
}
fun getDateWeek(day:Int):String{
    var dayWeek=""
    when(day){
        Calendar.MONDAY ->{
            dayWeek = "Lunes"
        }
        Calendar.TUESDAY ->{
            dayWeek = "Martes"
        }
        Calendar.WEDNESDAY ->{
            dayWeek = "Miercoles"
        }
        Calendar.THURSDAY ->{
            dayWeek = "Jueves"
        }
        Calendar.FRIDAY ->{
            dayWeek = "Viernes"
        }
        Calendar.SATURDAY ->{
            dayWeek = "Sabado"
        }
        Calendar.SUNDAY ->{
            dayWeek = "Domingo"
        }
    }
    return dayWeek
}

fun getMonth(month:Int):String{
    var dayWeek=""
    when(month){
        0 ->{
            dayWeek = "Enero"
        }
        1 ->{
            dayWeek = "Febrero"
        }
        2 ->{
            dayWeek = "Marzo"
        }
        3 ->{
            dayWeek = "Abril"
        }
        4 ->{
            dayWeek = "Mayo"
        }
        5 ->{
            dayWeek = "Junio"
        }
        6 ->{
            dayWeek = "Julio"
        }
        7 ->{
            dayWeek = "Agosto"
        }
        8 ->{
            dayWeek = "Septiembre"
        }
        9 ->{
            dayWeek = "Octubre"
        }
        10 ->{
            dayWeek = "Noviembre"
        }
        11 ->{
            dayWeek = "Noviembre"
        }
    }
    return dayWeek
}