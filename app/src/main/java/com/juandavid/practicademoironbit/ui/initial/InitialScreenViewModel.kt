package com.juandavid.practicademoironbit.ui.initial

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.practicademoironbit.data.model.array_movies.Movies
import com.juandavid.practicademoironbit.data.model.movie_search.MovieQuery
import com.juandavid.practicademoironbit.domain.GetMoviesArrayUsesCase
import com.juandavid.practicademoironbit.domain.GetMoviesQueryArrayUsesCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
/*
* ViewModel de Fragmento Inicial en App
* */
@HiltViewModel
class InitialScreenViewModel @Inject constructor(private val getMoviesArrayUsesCase: GetMoviesArrayUsesCase, private val getMoviesQueryArrayUsesCase: GetMoviesQueryArrayUsesCase) : ViewModel() {
    val arrayMovies = MutableLiveData<ArrayList<Movies>?>()
    val arrayMoviesQuery = MutableLiveData<ArrayList<MovieQuery>?>()
    val isLoading = MutableLiveData<Boolean>()
    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getMoviesArrayUsesCase.invoke()
            arrayMovies.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun arraySearchMovies(text:String) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getMoviesQueryArrayUsesCase.invoke(text)
            arrayMoviesQuery.postValue(result)
            isLoading.postValue(false)
        }
    }

}