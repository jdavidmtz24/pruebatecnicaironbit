package com.juandavid.practicademoironbit.ui

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.activity.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.juandavid.practicademoironbit.R
import com.juandavid.practicademoironbit.databinding.ActivityMainBinding
import com.juandavid.practicademoironbit.utls.getDateMobile
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

/*
* Actividad Inicial en App
* */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val viewModel: MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState == null) {

            setSupportActionBar(binding.toolbar)
            binding.toolbar.title= getDateMobile()
            val manager =
                applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            viewModel.deviceTablet.value = Objects.requireNonNull(manager).phoneType == TelephonyManager.PHONE_TYPE_NONE
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.containerFragment) as NavHostFragment
                NavigationUI.setupActionBarWithNavController(this, navHostFragment.navController)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val search = menu?.findItem(R.id.action_search)
        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Buscar"
        binding.toolbar.title = getDateMobile()
        searchView.setOnClickListener {
            binding.toolbar.title = ""
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    Log.e("onQueryTextSubmit",query)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    viewModel.textQueryClose.value = false
                    Log.e("onQueryTextChange",newText)
                    if (newText.length>=4){
                        viewModel.textQuery.value = newText
                       // toolbarCommunicator.passTextToolbar(newText)
                    }
                }
                return true
            }
        })
        searchView.setOnCloseListener {
            binding.toolbar.title = getDateMobile()
            viewModel.textQueryClose.value = true
            false
        }
        return super.onCreateOptionsMenu(menu)
    }

}