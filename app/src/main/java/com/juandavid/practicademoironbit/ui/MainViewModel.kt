package com.juandavid.practicademoironbit.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/*
* ViewModel de Actividad Principal
* */
@HiltViewModel
class MainViewModel @Inject constructor():ViewModel() {
    val textQuery = MutableLiveData<String>()
    val textQueryClose = MutableLiveData<Boolean>()
    val deviceTablet = MutableLiveData<Boolean>()
}