package com.juandavid.practicademoironbit.ui.detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.telephony.TelephonyManager
import android.text.Html
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.practicademoironbit.PracticaDemoIronBitApplication
import com.juandavid.practicademoironbit.R
import com.juandavid.practicademoironbit.adapter.CastMovieAdapter
import com.juandavid.practicademoironbit.databinding.FragmentDetailBinding
import com.juandavid.practicademoironbit.ui.MainActivity
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.util.*

/*
* Fragment Detalle de Show o Pelicula
* */
@AndroidEntryPoint
class DetailFragment : Fragment() {
    private var idMovie:Int? = 0

    companion object {
        fun newInstance() = DetailFragment()
    }

    private lateinit var viewModel: DetailViewModel
    private lateinit var binding: FragmentDetailBinding

    private lateinit var activityMain: MainActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMain = activity as MainActivity
        viewModel = ViewModelProvider(this).get(DetailViewModel::class.java)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activityMain.binding.toolbar.isVisible = false
        Log.e("Landscape","${activityMain.viewModel.deviceTablet.value}")
        if (activityMain.viewModel.deviceTablet.value!!){
            activityMain.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        idMovie = arguments?.getInt("idItem")
        Log.e("idItem", idMovie.toString())
        binding.buttonWebMovie.isVisible = false
        idMovie?.let {
            viewModel.onCreate(it)
        }
        viewModel.isLoading.observe(viewLifecycleOwner){
            binding.progressbarDetailMovie.isVisible = it
        }
        loadDetailMovie()
        showProgress()
    }

    private fun showProgress() {
        viewModel.isLoading.observe(viewLifecycleOwner){
            binding.progressbarDetailMovie.isVisible = it
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            var layoutInflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE)

        //binding.// FragmentDetailBinding.inflate(R.layout.fragment_detail, false)
           // binding.root
        }
    }

    private fun loadDetailMovie() {
        viewModel.movie.observe(viewLifecycleOwner){
            if (it != null) {
                binding.progressbarImageDetailMovie.isVisible = true
                val urlImage = if (activityMain.viewModel.deviceTablet.value!!) it.image!!.original else it.image!!.medium
                Picasso.get().load(urlImage).into(binding.imageviewDetailMovie, object :Callback{
                    override fun onSuccess() {
                        binding.progressbarImageDetailMovie.isVisible = false
                    }

                    override fun onError(e: Exception?) {
                        binding.progressbarImageDetailMovie.isVisible = false
                    }
                })
                binding.textviewNameDetailMovie.text = it.name
                binding.textviewNetworkNameDetailMovie.text = if (it.network!!.name != null) it.network!!.name else it.webChannel!!.name
                binding.textviewRatingAverageDetailMovie.text = if (it.rating!!.average != null) "Rating: ${it.rating!!.average}" else "No disponible"
                val sumaryHtml = Html.fromHtml(it.summary)
                binding.textviewSumarySinopsisDetailMovie.text = sumaryHtml
                if (it.genres.size >= 1) {
                    var genresConcat = ""
                    it.genres.forEach{genre->
                        genresConcat = "$genresConcat$genre, "
                    }
                    binding.textviewGenrDescriptionDetailMovie.text = genresConcat
                }else{
                    binding.textviewGenrDescriptionDetailMovie.text = it.type
                }
                var horario = it.schedule!!.time +" | ${it.schedule!!.days.toString().replace("[","").replace("]","")}"
                binding.textviewTimeDescriptionTitleDetailMovie.text = horario
                binding.progressbarImageDetailMovie.isVisible = false
                binding.buttonWebMovie.isVisible = true
                val uri = if (it.officialSite != null) Uri.parse(it.officialSite) else Uri.parse(it.url)
                binding.buttonWebMovie.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = uri
                    startActivity(intent)
                }
            }
        }
        viewModel.castActorArray.observe(viewLifecycleOwner){
            if (it != null) {
                val castMovieAdapter = CastMovieAdapter(it)
                binding.recyclerCastMovie.adapter = castMovieAdapter
                binding.recyclerCastMovie.layoutManager = LinearLayoutManager(context,RecyclerView.HORIZONTAL,false)
                binding.recyclerCastMovie.setHasFixedSize(true)
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        if (activityMain.viewModel.deviceTablet.value!!) {
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

}