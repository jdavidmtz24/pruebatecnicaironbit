package com.juandavid.practicademoironbit.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.practicademoironbit.data.model.cast.CastActors
import com.juandavid.practicademoironbit.data.model.movie.Movie
import com.juandavid.practicademoironbit.domain.GetCastActorsMovieIdUsesCase
import com.juandavid.practicademoironbit.domain.GetMovieIdUsesCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
/*
* ViewModel de Fragmento Detalla de Show o Pelicula
* */
@HiltViewModel
class DetailViewModel @Inject constructor(private val getMovieIdUsesCase: GetMovieIdUsesCase,
                                          private val getCastActorsMovieIdUsesCase: GetCastActorsMovieIdUsesCase) : ViewModel() {

    val isLoading = MutableLiveData<Boolean>()
    val movie = MutableLiveData<Movie?>()
    val castActorArray= MutableLiveData<ArrayList<CastActors>?>()
    fun onCreate(idMovie: Int) {
        isLoading.postValue(true)
        if (idMovie >= 1){
            viewModelScope.launch {
                val resultMovie = getMovieIdUsesCase.invoke(idMovie)
                val resultCastMovie = getCastActorsMovieIdUsesCase.invoke(idMovie)
                if (resultMovie!=null && resultCastMovie!= null) {
                    movie.postValue(resultMovie)
                    castActorArray.postValue(resultCastMovie)
                }
                isLoading.postValue(false)
            }
        }
        //isLoading.postValue(false)
    }


}