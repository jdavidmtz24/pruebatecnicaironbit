package com.juandavid.practicademoironbit.ui.initial


import android.content.pm.ActivityInfo
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.juandavid.practicademoironbit.R
import com.juandavid.practicademoironbit.adapter.MovieAdapter
import com.juandavid.practicademoironbit.adapter.MovieQueryAdapter
import com.juandavid.practicademoironbit.databinding.FragmentInitialScreenBinding
import com.juandavid.practicademoironbit.ui.MainActivity
import dagger.hilt.android.AndroidEntryPoint

/*
* Fragmento Principal enlista Show y/o peliculas del dia
* */
@AndroidEntryPoint
class InitialScreenFragment : Fragment(){

    companion object {
        fun newInstance() = InitialScreenFragment()
    }

    private lateinit var viewModel: InitialScreenViewModel
    private lateinit var searchView:SearchView
    private lateinit var binding: FragmentInitialScreenBinding
    private lateinit var activityMain: MainActivity


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMain = activity as MainActivity
        viewModel = ViewModelProvider(this)[InitialScreenViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        activityMain.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activityMain.binding.toolbar.isVisible = true
        binding = FragmentInitialScreenBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onCreate()
   /*     activityMain.setSupportActionBar(binding.toolbar)
        binding.toolbar.title="Martes 06 de febrero 2023"*/
        showProgress()
        dataRecyclerView()
        searchMoviesQuery()
        dataRecyclerSearchMovie()
        refreshData()
        closeSearchView()
    }

    private fun closeSearchView() {
        activityMain.viewModel.textQueryClose.observe(viewLifecycleOwner){
            if(it){
                dataRecyclerView()
                activityMain.viewModel.textQueryClose.value = false
            }
        }
    }

    private fun refreshData() {
        binding.swipeRefreshInitialScreen.setOnRefreshListener {
            dataRecyclerView()
            binding.swipeRefreshInitialScreen.isRefreshing = false
        }
    }

    private fun showProgress() {
        viewModel.isLoading.observe(viewLifecycleOwner){
            binding.progressInitialRecycler!!.isVisible = it
        }
    }
    private fun dataRecyclerView() {
        viewModel.arrayMovies.observe(viewLifecycleOwner){
            if (!it.isNullOrEmpty()){
                binding.recyclerMovies.removeAllViews()
                val arrayMoviesAdapter = MovieAdapter(it,MovieAdapter.OnClickListener{ modelMovies ->
                    Log.e("OnClickListener", modelMovies.show.toString())
                    modelMovies.show!!.id?.let { id -> onNavigateDetail(id) }
                })
                binding.recyclerMovies.adapter= arrayMoviesAdapter
                binding.recyclerMovies.layoutManager = LinearLayoutManager(context)
                binding.recyclerMovies.setHasFixedSize(true)
            }

        }
    }
    private fun dataRecyclerSearchMovie() {
        viewModel.arrayMoviesQuery.observe(viewLifecycleOwner){
            Log.e("arrayMoviesQuery", it.toString())
            if (it != null) {
                binding.recyclerMovies.removeAllViews()
                val arrayMovieQueryAdapter = MovieQueryAdapter(it,MovieQueryAdapter.OnClickListener{movieQuery ->
                    Log.e("OnClickListener", movieQuery.toString())
                    //modelMovies.show!!.id?.let { id -> onNavigateDetail(id)
                    movieQuery.show!!.id?.let { id-> onNavigateDetail(id)
                    }
                })
                binding.recyclerMovies.adapter= arrayMovieQueryAdapter
                binding.recyclerMovies.layoutManager = LinearLayoutManager(context)
                binding.recyclerMovies.setHasFixedSize(true)
            }
        }
    }

    private fun searchMoviesQuery() {
        activityMain.viewModel.textQuery.observe(viewLifecycleOwner){
            if (it !=null){
                Log.e("textToolbar", it.toString())
                if (it.length >= 4){
                    Log.e("textToolbar", "Mayor a 4")
                    viewModel.arraySearchMovies(it)
                }
            }
        }
    }


   private fun onNavigateDetail(idMovie:Int){
       val bundle = Bundle()
       bundle.putInt("idItem",idMovie)
       view?.findNavController()?.navigate(R.id.action_initialScreen_to_detailFragment, bundle)
   }




}