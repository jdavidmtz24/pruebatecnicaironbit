# PruebaTecnicaIronbit	

## Comandos de Clonacion de Repositorio

```
cd existing_repo
git remote add origin https://gitlab.com/jdavidmtz24/pruebatecnicaironbit.git
git branch -M main
git push -uf origin main
```
## Implementaciones en Proyecto

En el proyecto se muestran los puntos solicitados por el cliente, en el se utilizo lo siguiente

- Lenguaje de Programacion Kotlin - Se utiliza el lenguaje de Programacion Kotlin en todo el proyecto 

- Databinding - Se utiliza el Databinding para la vinculacion de componentes IU mediante formato declarativo

- ThemeDark - Se crea estilo de colores para modo Oscuro y Claro
 
- Patron de diseño MVVM - donde se agregan peticiones en red consumiendo un API [TV MAZE](https://www.tvmaze.com/api#full-schedule) con Retrofit 2 y corrutinas. También se añadido clean architecture.

- Dagger Hilt - Inyección de dependencias - Se ha añadido inyección de dependencias en todo el proyecto con Dagger Hilt 

- Orientacion de Pantalla - Se agregan plantillas para la rotacion de pantalla en Tabletas en la Vista de Detalle

## Pantallas de Muestra de la App Tablet

- Modo Claro

- Pantalla Inicio

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8AX90YpEnK-84_WXbUnW-Mv3virlwEG-ttmTFk0u3YX3etgZaLD4P3kFbdYy8JLQlhzJmbaiDN1JOLB4lgNBp3XS5KGD1oF3a2RI22rt30gl46jdcFDDxcvlpyddwaCuuR4zjkw6cj6aIiMsMUrPqCSIw=w800-h1280-no?authuser=0" alt="PantallaInicioTablet"></a>
</p>

- Pantalla Detalle

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8CjXmZVUWaSel1B2E5jy5p0wNJapzZu3LjsOXyzyuG-J9B3D20Kp8oMvrr-e-jVZUOTjmnpYPlYsoiki7LsTiR8LtTFs_G6ZgNJeNwFP4X43AG5rohhcLelDhkBdST7mHW2amKF8iyfKiXJPc2Ny4qBFw=w1280-h800-no?authuser=0" alt="PantallaDetalleTablet"></a>
</p>

---

- Modo Oscuro

- Pantalla Inicio

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8CDpE0NKkjuMSbOfS1gdN6SSQyEU6y_DMLmcG-1EI-xKtuUIk0aCaoYb77GZNHWD3JX_DW9jFLcPTIsNpyCBz5mvpvZqD0gzLvPJ19g5vre5LITE7cJtbJJs98dLa2xfZ47r2QBoh1MwSGUrVyK-fKNOg=w800-h1280-no?authuser=0" alt="PantallaInicioTablet"></a>
</p>

- Pantalla Detalle

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8DAf5v83_m7eXgKh3e-G4ugdaC0zRMkZoElTyiCO28CsPZSqBcbj9OkNG459o6jjfmFTF1i4W-JUpIA8oAYTJM6eCZXccEpyzC6_a17OfIez01D1qJ_K2Ch8AO1qNlXpD4wkh9wDrAmTQFzPmiGI4vczw=w1280-h800-no?authuser=0" alt="PantallaDetalleTablet"></a>
</p>


## Pantallas de Muestra de la App Movil

- Modo Claro

- Pantalla Inicio

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8BY7PLmtX0RalYBBnu5YjWVsiU_pbBurSiuGOYh5xbYLHPxG62UhPgx2Z8c67dxaUuYfHmQ9bDL8oR_bf6AHj_16dcZW0lCJIgUPbi4CiA16ND8HuNjgwBHzPMw372K30DODu7YFQYi1f9QbdXLyJkxEg=w480-h960-no?authuser=0" alt="PantallaInicioMovil"></a>
</p>

- Pantalla Detalle

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8BFXYBvZBj9fjtM37UJQWM5d317QPF7fLV58jTUbjsEDW_69_zhj5J6WBkf82ahHsxnN08lJYTnHvBmogavtoh4yAXM18FGngR6joSO47dojw6ojiaLX3lNtlTLi8ZpRPe4dPCV_5m3cBu73rlLHDfLRw=w480-h960-no?authuser=0" alt="PantallaDetalleMovil"></a>
</p>

---

- Modo Oscuro

- Pantalla Inicio

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8CFJOr8z1oOezVu8nD076cGhvJ2KGgggnnm8QqjT1K2bE1UZyunGjntaqaRQ3NZ5tWmrN9Bp9l2gtZVLT16U5wyHYGO1vVPJeVxWNSWUIsO_ocWmhPdgy6ZdkchPGKIxoQmHZ_tDoM2sza-HQGZdHnXXA=w480-h960-no?authuser=0" alt="PantallaInicioMovil"></a>
</p>

- Pantalla Detalle

<p align="center">
<a><img src="https://lh3.googleusercontent.com/pw/AMWts8AZVySeBBYprkH9JvC-nJswA-XvoGOgn1uQcoeVQOocIeiEg9Ql5bWA_6ZfMJv6fpvszrEe0EGLsBiekkRDXeDs8wNMlbFFsZRsh-1ngoPFwmF8IM0nerwkmLHs6TQGQzsya4Xp8w8jPS2BnXYtZTyklg=w480-h960-no?authuser=0" alt="PantallaDetalleMovil"></a>
</p>



